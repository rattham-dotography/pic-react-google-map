import React from 'react'
import ReactDOM from 'react-dom'
import R from 'ramda'
import Timer from '../sample-stateless/timer'

import {GoogleMapLoader, GoogleMap, Marker} from "react-google-maps";



export function randomGeo(center, radius) {
    var y0 = center.latitude;
    var x0 = center.longitude;
    var rd = radius / 111300; //about 111300 meters in one degree

    var u = Math.random();
    var v = Math.random();

    var w = rd * Math.sqrt(u);
    var t = 2 * Math.PI * v;
    var x = w * Math.cos(t);
    var y = w * Math.sin(t);

    //Adjust the x-coordinate for the shrinking of the east-west distances
    var xp = x / Math.cos(y0);

    var newlat = y + y0;
    var newlon = x + x0;
    var newlon2 = xp + x0;

    return {
      position: {
        lat: parseFloat(newlat.toFixed(8)),
        lng: parseFloat(newlon.toFixed(8))
      },
      key: `TA-${newlat.toFixed(5)}`,
      defaultAnimation: 2
    }
}

class SimpleMapWithState extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      markers: [{
        position: {
          lat: 25.0112183,
          lng: 121.52067570000001,
        },
        key: `Taiwan`,
        defaultAnimation: 2,
      }]
    }

  }

  componentDidMount() {
    setInterval(() => {
      let random = randomGeo({ latitude: 25.0112183, longitude: 121.52067570000001 }, 20000)
      const markers = R.concat(this.state.markers, random)
      this.setState({
        markers
      })
    }, 2000)
  }

  render() {
    const props = this.props;
    return (
      <section style={{height: "100%", width: "400px"}}>
        <GoogleMapLoader
          containerElement={
            <div
              {...props.containerElementProps}
              style={{
                height: "400px",
              }}
            />
          }
          googleMapElement={
            <GoogleMap
              ref={(map) => (this._googleMapComponent = map)}
              defaultZoom={9}
              defaultCenter={{ lat: 25.0112183, lng: 121.52067570000001 }}
              onClick={props.onMapClick}
            >
            {this.state.markers.map((marker, index) => {
              return (
                <Marker
                  {...marker}
                />
              );
            })}
            </GoogleMap>
          }
        />
      </section>
    );
  }
}

class SimpleMap2 extends React.Component {

  render() {
    const props = this.props;
    return (
      <section style={{height: "100%", width: "400px"}}>
        <GoogleMapLoader
          containerElement={
            <div
              {...props.containerElementProps}
              style={{
                height: "350px",
              }}
            />
          }
          googleMapElement={
            <GoogleMap
              ref={(map) => (this._googleMapComponent = map)}
              defaultZoom={13}
              defaultCenter={{ lat: 25.0112183, lng: 121.52067570000001 }}
              onClick={props.onMapClick}
            >
            {props.markers.map((marker, index) => {
              return (
                <Marker
                  {...marker}
                />
              );
            })}
            </GoogleMap>
          }
        />
      </section>
    );
  }

}

const SimpleMap = (props) => {
  return (
    <section style={{height: "100%", width: "250px"}}>
      <GoogleMapLoader
        containerElement={
          <div
            {...props.containerElementProps}
            style={{
              height: "250px",
            }}
          />
        }
        googleMapElement={
          <GoogleMap
            defaultZoom={10}
            defaultCenter={{ lat: 25.0112183, lng: 121.52067570000001 }}
            onClick={props.onMapClick}
          >
          {props.markers.map((marker, index) => {
              return (
                <Marker
                  {...marker}
                />
              );
            })}
          </GoogleMap>
        }
      />
    </section>
  );
}

const App = appState => (
  <div className="container">
    <h1>Stateless Component</h1>
    <Timer secondsElapsed={appState.secondsElapsed} />
    <SimpleMap markers={appState.markers}/>
    <SimpleMap2 markers={appState.markers}/>
    <h1>Stateful Component</h1>
    <SimpleMapWithState />
  </div>
)

App.render = R.curry((node, props) => {
  return ReactDOM.render(<App {...props} />, node)
})

export default App
