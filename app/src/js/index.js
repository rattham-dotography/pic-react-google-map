'user strict';
/*import 'bootstrap'
import React from 'react'
import { render } from 'react-dom'
import Root from './containers/Root'

render(
  <Root />,
  document.getElementById('app')
);
*/

//import App from './sample-stateless/app'
import App, { randomGeo } from './google_map'
import R from 'ramda'



let appState = {
  secondsElapsed: 0,
  todos: [
    {id: 1, text: 'Buy milk'},
    {id: 2, text: 'Go running'},
    {id: 3, text: 'Rest'}
  ],
  markers: [{
    position: {
      lat: 25.0112183,
      lng: 121.52067570000001,
    },
    key: `Taiwan`,
    defaultAnimation: 2,
  }]
}

const render = App.render(document.getElementById('app'))
//first render
render(appState)

const secondsElapsedLens = R.lensProp('secondsElapsed');
const incSecondsElapsed = R.over(secondsElapsedLens, R.inc);

const markersLens = R.lensProp('markers');
const addMarkers = R.over(markersLens, markers => {
  let random = randomGeo({ latitude: 25.0112183, longitude: 121.52067570000001 }, 2000)
  return R.concat(markers, random)
})

setInterval(() => {
  appState = R.compose(incSecondsElapsed, addMarkers)(appState);
  render(appState)
}, 2000);

